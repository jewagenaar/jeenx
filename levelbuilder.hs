module LevelBuilder where

import System.IO
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as B8
import Data.Binary
import Data.Primitive.MachDeps
import Data.Int
import Jeenx.Data
import Jeenx.Tokens

-- generic list encoding function
doEncode :: [a] -> (a -> B.ByteString) -> [B.ByteString] -> B.ByteString
doEncode []     f b = B.concat b
doEncode [x]    f b = doEncode [] f $ b ++ [(f x)]
doEncode (x:xs) f b = doEncode xs f $ b ++ [(f x)] 

-- word token
newWordToken :: Int -> Int -> String -> WordToken
newWordToken t c s = Wt t c s

wt_encode :: WordToken -> B.ByteString
wt_encode w = B.concat [(encode ((Prelude.length $ text w)::Int)),
                        (encode (token w :: Int)),
                        (encode (token_class w :: Int)),
                        (B8.pack (text w))]
              
-- instructions 
newInstr :: Int -> Int -> String -> Instr
newInstr v n p = Instr v n p

in_encode :: Instr -> B.ByteString
in_encode i = B.concat [(encode ((Prelude.length $ performed i)::Int)),
                        (encode (verb i)),
                        (encode (noun i)),
                        (B8.pack (performed i))]

putInstr :: Handle -> Instr -> IO ()
putInstr h i = do
  B.hPut h (in_encode i)
              
dm_encode :: Domain -> B.ByteString
dm_encode d = B.concat [(encode ((Prelude.length $ desc d)::Int)),
                        (B8.pack (desc d)),
                        (encode ((Prelude.length $ instructs d)::Int)),
                        (doEncode (instructs d) (in_encode) [])]

header_encode :: DatHeader -> B.ByteString
header_encode d = B.concat [(encode $ verbCount d),
                            (encode $ nounCount d),
                            (encode $ domainCount d)]

-------------------------------------
-- write a test out file
createLevelFile :: String -> Game -> IO ()
createLevelFile fn g = do
  h <- openFile fn WriteMode
  let header = DH (Prelude.length $ nouns g) (Prelude.length $ verbs g) (Prelude.length $ domains g)
  B.hPut h (header_encode header)
  let nbs = doEncode (nouns g) (wt_encode) []
  B.hPut h nbs
  let vbs = doEncode (verbs g) (wt_encode) []
  B.hPut h vbs
  let dbs = doEncode (domains g) (dm_encode) []
  B.hPut h dbs
  hClose h
