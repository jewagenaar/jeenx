module Jeenx where

import Jeenx.Data
import Jeenx.Tokens
import System.IO

import qualified Data.Char as DC
import Data.List
import Data.Maybe

-- main method                    
runJeenx :: State -> Game -> IO ()
runJeenx s g = do
  putStr "Good day, and welcome to our world, mysterious traveller. "
  putStrLn "Perhaps it would be a good idea to have a quick *look* around... "
  cmd <- getNextInput s g
  processCmds cmd g

-- process an input line
processInput :: String -> State -> Game -> Cmd
processInput l s g = processTokens (map (\x -> tokenize (map DC.toLower x) w) $ words l) s
  where w = (nouns g) ++ (verbs g)

-- process tokens
processTokens :: [WordToken] -> State -> Cmd
processTokens [] state         = Cmd cmd_what "you have to tell me what to do!" state
processTokens (x:xs) state
  | (token_class x) == tc_sysc = processSysCmd x state
  | (token_class x) == tc_verb = processVerb x xs state
processTokens tokens state     = (let error_str = ("i don't know what '" ++ (untokenize tokens) ++ "' means") in
                                   Cmd cmd_what error_str state)

-- process a system command
processSysCmd :: WordToken -> State -> Cmd
processSysCmd w s
  | (token w) == t_quit = Cmd cmd_quit "user quit" s
  | (token w) == t_help = Cmd cmd_help "user needs help" s
  | otherwise           = error $ "can't process the word token" ++ (show w)

-- process a verb
processVerb :: WordToken -> [WordToken] -> State -> Cmd
processVerb v t s
  | (token v) == t_look = Cmd cmd_desc "user looks around"    s
  | (token v) == t_help = Cmd cmd_help "user needs some help" s
  | otherwise           = case checkValidInstruction v t s of
    True  -> Cmd cmd_next "good. get next." s
    False -> Cmd cmd_what ("what exactly is it you want to " ++ (text v) ++ "?") s

-- check if this is a valid instruction for the current state
checkValidInstruction :: WordToken -> [WordToken] -> State -> Bool
checkValidInstruction _ [] _ = False                               -- verbs must be followedd by a noun
checkValidInstruction v t s
  | (token_class $ head t) == tc_verb                     = False  -- verbs can't be followed by another verb
  | (token v) == (verb i) && (token $ head t) == (noun i) = True   -- this is good match
  | otherwise = checkValidInstruction v (tail t) s                 -- drill down, looking for a valid noun
  where i = head $ instructs $ domain s

-- process the commands
processCmds :: Cmd -> Game -> IO ()
processCmds cmd g
  | (index cmd) == cmd_quit = do
    performQuit cmd
  | (index cmd) == cmd_what = do
    performWhat cmd g
  | (index cmd) == cmd_desc = do
    performDesc cmd g
  | (index cmd) == cmd_next = do
    performNext cmd g
  | (index cmd) == cmd_help = do
    performHelp cmd g
  | otherwise = error $ "can't handle command " ++ (meta cmd)

-- look around the current domain
performDesc :: Cmd -> Game -> IO ()
performDesc cmd g = do
  putStrLn (desc $ domain $ (state cmd))
  n <- getNextInput (state cmd) g
  processCmds n g
                   
-- step the state forward
performNext :: Cmd -> Game -> IO ()
performNext cmd g = (let s = updateState (state cmd) (domains g) in
                      case isJust s of
                        True -> do
                          putStrLn "..."
                          putStrLn (desc $ domain $ (fromJust s))
                          n <- getNextInput (fromJust s) g
                          processCmds n g
                        False -> do
                          performQuit (Cmd cmd_quit "end of inputs." null_state))

-- quit the program
performQuit :: Cmd -> IO ()
performQuit cmd = do
  putStrLn "... and so your journey comes to an end. Farewell."
  return ()

-- tell the user that we don't know what he wants
performWhat :: Cmd -> Game -> IO ()
performWhat cmd g = do
  putStrLn "i'm not sure what you want from me..."
  putStrLn (meta cmd)
  n <- getNextInput (state cmd) g
  processCmds n g

-- the user want's some help
performHelp :: Cmd -> Game -> IO ()
performHelp cmd g = do
  putStrLn "You can look around by typing something like \"Look around myself\" or just \"look\" (if you're lazy). "
  putStrLn "If you want to get back to quit simply type \":q\" and hit the [Return] key."
  putStrLn "Good luck!"
  n <- getNextInput (state cmd) g
  processCmds n g

-- prompts the user for the next input
getNextInput :: State -> Game -> IO Cmd
getNextInput s g = do
  putStr ">> "
  hFlush stdout
  l <- getLine
  return (processInput l s g)

  