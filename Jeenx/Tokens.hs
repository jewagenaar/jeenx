module Jeenx.Tokens where

import Jeenx.Data
import Data.List
import Data.Maybe

-- global tokens
t_quit = -1 :: Token
t_look = -2 :: Token
t_help = -3 :: Token

-- global word tokens
wt_quit = (Wt t_quit tc_verb "quit")
wt_look = (Wt t_look tc_verb "look")
wt_help = (Wt t_help tc_verb "help")

-- create a token from a string
tokenize :: String -> [WordToken] -> WordToken
tokenize str tokens
  | (tokenizeCmd str)  /= Nothing = (Wt (fromJust $ tokenizeCmd str) tc_sysc str)
  | tokenized          /= Nothing = fromJust tokenized
  | otherwise           = (Wt t_quit tc_word str)
  where tokenized = (tokenizeWord str tokens)

-- create tokens from system command string values
tokenizeCmd :: String -> Maybe Token
tokenizeCmd str
  | ":q" == str = Just t_quit
  | otherwise   = Nothing

-- create a noun token from a word
tokenizeWord :: String -> [WordToken] -> Maybe WordToken
tokenizeWord str tokens
  | null matches = Nothing
  | otherwise    = Just (head matches)
  where matches = filter (\x -> text x == str) tokens

-- undo tokenization
untokenize :: [WordToken] -> String
untokenize []     = ""
untokenize [x]    = (text x)
untokenize tokens = concat (intersperse " " (map (\x -> (text x)) tokens))