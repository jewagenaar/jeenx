module Jeenx.Data where

import Data.List
import Data.Maybe
 
-- TOKENS
type Token = Int

tc_sysc = 0 :: Int
tc_verb = 1 :: Int
tc_noun = 2 :: Int
tc_word = 3 :: Int

-- WORDS 
data WordToken = Wt { token :: Token,  token_class :: Int, text :: String }

instance Show WordToken where
  show w = show ()

newWordToken :: Int -> Int -> String -> WordToken
newWordToken t tc s = Wt t tc s

-- equals implementation for WordTokens
instance Eq WordToken where
  t1 == t2 = ((token t1) == (token t2) && (token_class t1) == (token_class t2))
  t1 /= t2 = ((token t1) /= (token t2) || (token_class t1) /= (token_class t2))

-- INSTRUCTIONS
data Instr = Instr { verb :: Token, noun :: Token, performed :: String }

newInstr :: Int -> Int -> String -> Instr
newInstr v n p = Instr v n p

-- DOMAIN
data Domain = Domain { desc :: String, instructs :: [Instr] }

-- method to create new domain
newDomain :: String -> [Instr] -> Domain
newDomain s i = (Domain s i)

-- update a domain                          
updateDomain :: State -> Domain
updateDomain s = newDomain ((desc $ domain s) ++ " " ++ (performed $ head $ instructs $ domain s))
                 (tail $ instructs $ domain s)

-- STATE
data State = State { bag :: [WordToken], domain :: Domain, domain_index :: Int }

-- method to create new state
newState :: [WordToken] -> Domain -> Int -> State
newState t d i = (State t d i)

-- update a state
updateState :: State -> [Domain] -> Maybe State
updateState s d
  | empty_queue && empty_domains == True = Nothing
  | empty_queue == True                  = Just (newState (bag s) (d !! next_index) next_index)
  | otherwise                            = Just (newState (bag s) (updateDomain s) (domain_index s))
  where empty_queue     = null $ tail $ instructs $ domain s
        empty_domains   = (length d) <= next_index
        next_index      = 1 + (domain_index s)

-- and empty state (null object)
null_state = (newState [] (newDomain "" []) 0)

-- COMMANDS
type CmdIndex = Int

cmd_quit = 0 :: CmdIndex -- quit 
cmd_next = 1 :: CmdIndex -- get next command
cmd_what = 2 :: CmdIndex -- don't know what user wants
cmd_desc = 3 :: CmdIndex -- describe the current state to the player
cmd_help = 4 :: CmdIndex -- the user needs some help

data Cmd = Cmd { index :: CmdIndex, meta :: String, state :: State }

-- GAME 
data Game = Game { nouns :: [WordToken], verbs :: [WordToken], domains :: [Domain] }

-- DATA FILE HEADER
data DatHeader = DH { verbCount :: Int, nounCount :: Int, domainCount :: Int } 

