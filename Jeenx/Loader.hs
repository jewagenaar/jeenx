module Jeenx.Loader where

import System.IO
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as B8
import Data.Binary
import Data.Primitive.MachDeps
import Data.Int

import Jeenx.Data
import Jeenx.Tokens

-- read domains from a file handle
readDomains :: Handle -> Int -> Int -> [Domain] -> IO [Domain]
readDomains h i c r
  | c == i    = return r
  | otherwise = do
    bl <- B.hGet h sIZEOF_INT
    let len = (decode bl) :: Int
    bs <- B.hGet h len
    let s = (B8.unpack bs)
    bn <- B.hGet h sIZEOF_INT
    let n = (decode bn) :: Int          
    is <- (readData h (newInstr) 0 n [])
    let d = Domain s is
    readDomains h (i+1) c (r ++ [d])

-- read in a data file header from the file handle
readHeader :: Handle -> IO DatHeader
readHeader h = do
  bn <- B.hGet h sIZEOF_INT
  bv <- B.hGet h sIZEOF_INT
  bd <- B.hGet h sIZEOF_INT
  let n = (decode bn) :: Int
  let v = (decode bv) :: Int
  let d = (decode bd) :: Int
  return (DH v n d)

-- generic read function: read some data of the form (Int -> Int -> String)
readData :: Handle -> (Int -> Int -> String -> a) -> Int -> Int -> [a] -> IO [a]
readData h f i c r
  | i == c = return r
  | otherwise = do
      bl <- B.hGet h sIZEOF_INT
      let len = (decode bl) :: Int
      bt <- B.hGet h sIZEOF_INT
      let t = (decode bt) :: Int
      bg <- B.hGet h sIZEOF_INT
      let g = (decode bg) :: Int          
      bs <- B8.hGet h len
      let s = (B8.unpack bs)
      let d = (f t g s)
      readData h f (i+1) c (r ++ [d])

-- load a .dat file
loadDataFile :: String -> IO Game
loadDataFile fn = do
  h <- openFile fn ReadMode
  b <- hIsEOF h
  case b of
    True  -> do
      return (error "unexpected EOF")
    False -> do
      dh <- readHeader h
      let nc = (nounCount dh)
      tn <- readData h (newWordToken) 0 nc []
      let vc = (verbCount dh)
      tv <- readData h (newWordToken) 0 vc []
      let dc = (domainCount dh)
      td <- readDomains h 0 dc []
      return (Game tn (wt_help:wt_quit:wt_look:tv) td)