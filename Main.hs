module Main where

import System.Environment

import Jeenx
import Jeenx.Data
import Jeenx.Loader

main :: IO ()
main = do
  args <- getArgs
  case (null args) of
    True  -> do
      putStrLn "you need to supply a .dat file as parameter"
    False -> do
      let fn = head args        -- get the filename
      g <- loadDataFile fn      -- load the data from the file
      let d = head $ domains g  -- get the first domain
      let s = newState [] d 0   -- create a start state
      runJeenx s g              -- and run the game