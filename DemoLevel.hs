module DemoLevel where

import Jeenx.Data
import Jeenx.Tokens

g_verbs = [(Wt 0 tc_verb "walk"   ),
           (Wt 1 tc_verb "open"   ),
           (Wt 2 tc_verb "close"  ),
           (Wt 3 tc_verb "pickup" ),
           (Wt 4 tc_verb "press"  ),
           (Wt 5 tc_verb "climb"  ),
           (Wt 6 tc_verb "leave"  )]

g_nouns = [(Wt 0  tc_noun "tree"   ),
           (Wt 1  tc_noun "road"   ),
           (Wt 2  tc_noun "book"   ),
           (Wt 3  tc_noun "door"   ),
           (Wt 4  tc_noun "room"   ),
           (Wt 5  tc_noun "hatch"  ),
           (Wt 6  tc_noun "stairs" ),
           (Wt 7  tc_noun "switch" ),
           (Wt 8  tc_noun "north"  ),
           (Wt 9  tc_noun "south"  ),
           (Wt 10 tc_noun "east"   ),
           (Wt 11 tc_noun "west"   )]

domain_1 = Domain
           ("You are in a very dimly lit room. " ++
            "There appears to be a large wooden door on the northen side of the room. ")
           [(Instr 0 8 "You've walked north, towards the door. The door is not locked. "),
            (Instr 1 3 "You've opened the door. You can now *leave the room*. "),
            (Instr 6 4 "")]
           
domain_2 = Domain
           ("You are now standing outside in a forest. " ++
            "There seems to be a path that leads up a hill to your right.")
           [(Instr 0 10 "You walk east towards the hill.")]

game = Game g_nouns g_verbs [domain_1, domain_2]